import java.io._

name := "mk-dummy-logic"
organization := "no.uit.sfb"

enablePlugins(GitVersioning)
useJGit
git.gitlabCiOverride := true

val updateDeploymentVersions = SettingKey[Unit](
  "update-deployment-version",
  "Update the deployment version")

updateDeploymentVersions := {
  val pw = new PrintWriter(new File(".version"))
  pw.write(version.value)
  pw.close()
}