#!/bin/bash

POSITIONAL=()
PIPELINE_PATH="."
PIPELINE_NAME=""
while [[ $# -gt 0 ]]
do
  case "$1" in
    -h|--help)
      echo "Setup application"
      echo ""
      echo "Usage"
      echo "-p <path_to_pipeline> -n <pipeline_name>"
      echo ""
      echo "[-h|--help]"
      echo "print this help message"
      exit 0
      ;;
    -p)
      PIPELINE_PATH=$2
      shift
      shift
      ;;
    -n)
      PIPELINE_NAME=$2
      shift
      shift
      ;;
    *) #Unknown
      POSITIONAL+=("$1")
      shift
      ;;
  esac
done

THIS_PATH="$( cd "$( dirname "$0" )" && pwd )"

VERSION=$(cat $THIS_PATH/../.version)

if [[ -z ${PIPELINE_NAME} ]]; then echo "Please set envir PIPELINE_NAME"; exit 1; fi

set -e

USER=$( sed -n -e '/^user=/p' ~/.sbt/.credentials | sed "s/^user=//")
PASSWORD=$( sed -n -e '/^password=/p' ~/.sbt/.credentials | sed "s/^password=//")
SUB_PIPELINE=$(basename ${PIPELINE_PATH})
rm -rf target
mkdir target
cd target
ARCHIVE="${PIPELINE_NAME}_${SUB_PIPELINE}.tgz"
cp -r ../templates templates
shopt -s globstar
sed -i "s/##VERSION##/$VERSION/g" templates/**/*.tpl.yml
tar -czvf "${ARCHIVE}" templates ../config.yaml
REPOSITORY="https://artifactory.metapipe.uit.no/artifactory/generic-local"
ARTIFACT="no.uit.sfb/metakube/pipelines/${PIPELINE_NAME}/${SUB_PIPELINE}"

ret=$(curl -u ${USER}:${PASSWORD} --silent --output /dev/null --write-out "%{http_code}" -T ${ARCHIVE} ${REPOSITORY}/${ARTIFACT})
if [[ ${ret} != "201" ]]; then echo "Failed to upload artifact ${ARTIFACT} (returned $ret)"; exit 1; fi
cd ..
